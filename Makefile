CC = gcc
CFLAGS = -g -Wall -O2

prefix = /usr/local
include xdg-shell.mk

libwaylandhelper.so: waylandhelper.c xdg-shell.c xdg-shell.h *.c *.h Makefile
	$(CC) $(CFLAGS) -fpic -shared -o $@ $< -lxkbcommon -lwayland-client

waylandhelper.o: waylandhelper.c xdg-shell.c xdg-shell.h *.c *.h Makefile
	$(CC) $(CFLAGS) -fpic -c $<

xdg-shell.mk:
	echo xdgshell = `find /usr/ -name xdg-shell.xml -print -quit 2>/dev/null` > $@

xdg-shell.h: $(xdgshell)
	wayland-scanner client-header $< $@

xdg-shell.c: $(xdgshell)
	wayland-scanner private-code $< $@

test.out: test.c waylandhelper.o
	$(CC) $(CFLAGS) -o $@ $^ -lwayland-client -lxkbcommon

clean:
	rm -f *.out *.o xdg-shell* *.so

install: libwaylandhelper.so
	cp $< $(prefix)/lib
	cp waylandhelper.h $(prefix)/include

uninstall:
	rm -f $(prefix)/lib/libwaylandhelper.so $(prefix)/include/waylandhelper.h
