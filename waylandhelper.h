#ifndef __waylandhelper__
#define __waylandhelper__
#include <xkbcommon/xkbcommon.h>
#include <wayland-client.h>
#include <stdint.h>

/* This enum is copied here from wlroots to avoid making that a dependency. */
enum wlr_keyboard_modifier {
	WLR_MODIFIER_SHIFT = 1 << 0,
	WLR_MODIFIER_CAPS = 1 << 1,
	WLR_MODIFIER_CTRL = 1 << 2,
	WLR_MODIFIER_ALT = 1 << 3,
	WLR_MODIFIER_MOD2 = 1 << 4,
	WLR_MODIFIER_MOD3 = 1 << 5,
	WLR_MODIFIER_LOGO = 1 << 6,
	WLR_MODIFIER_MOD5 = 1 << 7,
};

struct wlh_backend {
	/* things to recieve */
	struct wl_display*    display;
	struct wl_registry*   registry;
	struct wl_compositor* compositor;
	struct wl_seat*       seat;
	/* things to create */
	struct wl_surface*	surface;
	struct wl_buffer*	buffer;
	struct wl_callback*	framecaller;

	struct xdg_wm_base* xdg_base;
	struct xdg_surface*   xdgsurf;
	struct xdg_toplevel*  xdgtop;
	struct xkb_context* xkbcontext;
	struct wl_keyboard* keyboard;
	int xdg_surface_changed, pending_x, pending_y, xdgfirst;

	struct wl_pointer *mouse;

	struct wl_shm* shared_memory;
	size_t imagebuffer_size;
};

struct waylandhelper {
	uint32_t* data;
	int xresmin, yresmin, stop, redraw, can_redraw,
		xres, yres, // only mutable in xdg:topconfigure
		res_changed; // user may set to zero if wants
	unsigned button;
	long long repeat_interval_µs, repeat_delay_µs, last_keytime_µs, last_repeat_µs;
	int last_key, last_keymods, keydown, mousex, mousey;
	char *title;
	void (*key_callback)(struct waylandhelper*);
	void (*motion_callback)(struct waylandhelper*, int xrel, int yrel);
	void (*wheel_callback)(struct waylandhelper*, int axistype, int scroll);
	void (*button_callback)(struct waylandhelper*, int button, int state);
	void (*resolution_callback)(struct waylandhelper*, int oldx, int oldy);

	struct xkb_state* xkbstate;
	void *userdata;
	int owner;
	struct wlh_backend backend;
};

long long wlh_timenow_µs();
unsigned wlh_get_modstate(const struct waylandhelper *wlh);
int wlh_key_should_repeat(struct waylandhelper *wlh);
void wlh_commit(struct waylandhelper *wlh);
void wlh_nofullscreen();
void wlh_destroy(struct waylandhelper *wlh);
void wlh_fullscreen();
void wlh_update_title(struct waylandhelper *wlh);
int wlh_init(struct waylandhelper *wlh);
int wlh_get_keysyms(const struct waylandhelper*, const xkb_keysym_t**syms);
int wlh_get_keytext(const struct waylandhelper*, char *out, int sizeout);
int wlh_roundtrip(struct waylandhelper *wlh);

/*
 * A simple mainloop can look like this:
 * while (!wlh.stop && wlh_roundtrip(&wlh) >= 0) {
 *	if (!wlh.can_redraw)
 *		goto next;
 *	if (wlh.redraw)
 *		your_drawing_function(wlh.data, wlh.xres, wlh.yres);
 *		wlh_commit(&wlh);
 *	}
 * next:
 *	usleep(10000);
 * }
 */

#endif
