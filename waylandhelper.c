#include <wayland-client.h>
#include <xkbcommon/xkbcommon-names.h>
#include "xdg-shell.h"
#include "xdg-shell.c"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include "waylandhelper.h"

#include "framecallback.c" // framecaller
#include "xdg.c" // xdg_base
#include "shm.c" // shared_memory
#include "keyboard.c"
#include "mouse.c"

#define do_binding if(0)
#define option(a,b,c) } else if(!strcmp(interface, a##_interface.name)) { b = wl_registry_bind(reg, id, &a##_interface, c)

static void registry_handler(void* data, struct wl_registry* reg, uint32_t id,
	const char* interface, uint32_t version) {
	struct waylandhelper *wlh = data;
	struct wlh_backend *b = &wlh->backend;
	do_binding {
		option(wl_compositor, b->compositor, 4);
		option(xdg_wm_base, b->xdg_base, 1),
			xdg_wm_base_add_listener(b->xdg_base, &xdg_base_listener, NULL);
		option(wl_shm, b->shared_memory, 1);
		option(wl_seat, b->seat, 8);
	}
}

#undef do_binding
#undef option

static void registry_destroyer(void* data, struct wl_registry* reg, uint32_t id) {
	wl_registry_destroy(((struct waylandhelper*)data)->backend.registry);
	((struct waylandhelper*)data)->backend.registry = NULL;
}

static struct wl_registry_listener registry_listener = {
	.global = registry_handler,
	.global_remove = registry_destroyer,
};

static int require(void* a, ...) {
	va_list vl;
	char* b;
	int has_error = 0;
	va_start(vl, a);
	do {
		b = va_arg(vl, char*);
		if (!a) {
			fprintf(stderr, "%s is missing\n", b);
			has_error = 1;
		}
	} while ((a=va_arg(vl, void*)));
	va_end(vl);
	return has_error;
}

int wlh_init(struct waylandhelper *wlh) {
	struct wlh_backend *b = &wlh->backend;
	assert((b->display = wl_display_connect(NULL)));
	assert((b->registry = wl_display_get_registry(b->display)));
	wl_registry_add_listener(b->registry, &registry_listener, wlh);
	wl_display_dispatch(b->display);
	wl_display_roundtrip(b->display);
	if (require(b->display, "display", b->compositor, "compositor", b->shared_memory,
			"shared_memory", b->xdg_base, "xdg_base", b->seat, "wl_seat", NULL))
		return 1;

	assert((b->surface = wl_compositor_create_surface(b->compositor)));
	b->framecaller = wl_surface_frame(b->surface);
	wl_callback_add_listener(b->framecaller, &frame_listener, wlh);

	wlh->redraw = 1;

	if (wlh->xresmin <= 0)
		wlh->xresmin = 1;
	if (wlh->yresmin <= 0)
		wlh->yresmin = 1;
	if (wlh->xres <= 0)
		wlh->xres = wlh->xresmin;
	if (wlh->yres <= 0)
		wlh->yres = wlh->yresmin;
	init_xdg(wlh);
	wlh_update_title(wlh);

	init_keyboard(wlh);
	wlh_init_mouse(wlh);
	return 0;
}

void wlh_destroy(struct waylandhelper *wlh) {
	struct wlh_backend *b = &wlh->backend;
	destroy_imagebuffer(wlh);
	if (b->buffer)
		wl_buffer_destroy(b->buffer);

	xdg_toplevel_destroy(b->xdgtop);
	xdg_surface_destroy(b->xdgsurf);

	wl_surface_destroy(b->surface);
	wl_callback_destroy(b->framecaller);

	if (b->keyboard)
		destroy_keyboard(wlh);

	if (b->mouse)
		wl_pointer_release(b->mouse);

	wl_seat_destroy(b->seat);
	wl_shm_destroy(b->shared_memory);
	xdg_wm_base_destroy(b->xdg_base);
	wl_compositor_destroy(b->compositor);
	wl_registry_destroy(b->registry);
	wl_display_disconnect(b->display);

	memset(b, 0, sizeof(*b));
	wlh->stop = 1;

	if (wlh->owner)
		free(wlh);
}

void wlh_fullscreen(struct waylandhelper *wlh) {
	xdg_toplevel_set_fullscreen(wlh->backend.xdgtop, NULL);
}

void wlh_nofullscreen(struct waylandhelper *wlh) {
	xdg_toplevel_unset_fullscreen(wlh->backend.xdgtop);
}

void wlh_commit(struct waylandhelper *wlh) {
	struct wlh_backend *b = &wlh->backend;
	wl_surface_damage_buffer(b->surface, 0, 0, wlh->xres, wlh->yres);
	wl_surface_attach(b->surface, b->buffer, 0, 0); // This is always released automatically.
	wl_surface_commit(b->surface);
	wlh->redraw = wlh->can_redraw = 0;
}

#define modbits(a) (WLR_MODIFIER_##a * !!xkb_state_mod_name_is_active(wlh->xkbstate, XKB_MOD_NAME_##a, XKB_STATE_MODS_EFFECTIVE))
unsigned wlh_get_modstate(const struct waylandhelper *wlh) {
	return modbits(CTRL) | modbits(ALT) | modbits(SHIFT) | modbits(LOGO);
}
#undef modbits

long long wlh_timenow_µs() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (long long)tv.tv_sec*1000000 + tv.tv_usec;
}

int wlh_get_keysyms(const struct waylandhelper *wlh, const xkb_keysym_t** syms) {
	return xkb_state_key_get_syms(wlh->xkbstate, wlh->last_key, syms);
}

int wlh_get_keytext(const struct waylandhelper *wlh, char *out, int sizeout) {
	return xkb_state_key_get_utf8(wlh->xkbstate, wlh->last_key, out, sizeout);
}

void wlh_update_title(struct waylandhelper *wlh) {
	xdg_toplevel_set_title(wlh->backend.xdgtop, wlh->title ? wlh->title : "");
}

int wlh_key_should_repeat(struct waylandhelper *wlh) {
	if (!wlh->keydown)
		return 0;
	long long now = wlh_timenow_µs();
	int ret;
	if (wlh->last_repeat_µs)
		ret = now - wlh->last_repeat_µs >= wlh->repeat_interval_µs;
	else
		ret = now - wlh->last_keytime_µs >= wlh->repeat_delay_µs;
	if (ret) {
		wlh->last_repeat_µs = now;
		return 1;
	}
	return 0;
}

int wlh_roundtrip(struct waylandhelper *wlh) {
	return wl_display_roundtrip(wlh->backend.display);
}
