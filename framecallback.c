static void framecallback(void*, struct wl_callback*, uint32_t);

static const struct wl_callback_listener frame_listener = {
	framecallback,
};

static void framecallback(void* data, struct wl_callback* callback, uint32_t time) {
	struct waylandhelper *wlh = data;
	wl_callback_destroy(wlh->backend.framecaller);
	wlh->backend.framecaller = wl_surface_frame(wlh->backend.surface);
	wl_callback_add_listener(wlh->backend.framecaller, &frame_listener, wlh);
	wlh->can_redraw = 1;
}
