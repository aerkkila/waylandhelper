#include <string.h>
#include <err.h>
#include <unistd.h>
#include "waylandhelper.h"

static int pause_drawing = 0;

static void key_callback(struct waylandhelper *wlh) {
	if (!wlh->keydown)
		return;
	const xkb_keysym_t *syms;
	int nsyms = wlh_get_keysyms(wlh, &syms);
	for (int isym=0; isym<nsyms; isym++) {
		switch (syms[isym]) {
			case XKB_KEY_q:
				wlh->stop = 1; break;
			case XKB_KEY_space:
				pause_drawing = !pause_drawing; break;
			default:
				break;
		}
	}
}

int main() {
	char title[40] = "test program";
	struct waylandhelper wlh = {
		.key_callback = key_callback,
		.xres = 500,
		.yres = 500,
		.title = title,
	};
	if (wlh_init(&wlh))
		errx(1, "wlh_init_wayland");
	int number = 0;
	int scale = 1;

	while (!wlh.stop && wlh_roundtrip(&wlh) > 0) {
		if (!wlh.can_redraw) {
			usleep(10000);
			continue;
		}
		if (pause_drawing) {
			usleep(10000);
			continue;
		}

		/* draw some random stuff */
		int maxx = wlh.xres/scale;
		int maxy = wlh.yres/scale;
		for (int j=0; j<maxy-scale; j++) {
			uint32_t *ptr = wlh.data + j*scale*wlh.xres;
			for (int i=0; i<=maxx-scale; i++) {
				uint32_t color =
					(number*2 & 0xff) |                     // red
					((j/2*(i/2) + number) & 0xff) << 8 |    // green
					((j/5*(i/4) + number*5) & 0xff) << 16 | // blue
					0xff << 24;                             // alpha
				ptr[i*scale] = color;
				for (int sx=1; sx<scale; sx++)
					ptr[i*scale+sx] = color;
			}
			for (int sy=1; sy<scale; sy++)
				memcpy(ptr+wlh.xres*sy, ptr, wlh.xres*4);
		}

		number++;
		sprintf(title, "test program: %i", number);
		wlh_update_title(&wlh);
		wlh_commit(&wlh);
		usleep(10000);
	}

	wlh_destroy(&wlh);
}
