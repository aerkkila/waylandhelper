static void xdg_wm_base_ping(void* data, struct xdg_wm_base *xwb, uint32_t ser) {
	xdg_wm_base_pong(xwb, ser);
}

static struct xdg_wm_base_listener xdg_base_listener = {
	.ping = xdg_wm_base_ping,
};

/*******************************/
static struct wl_buffer* attach_imagebuffer(struct waylandhelper*); // from shm.c

static void xdgconfigure(void* data, struct xdg_surface* surf, uint32_t serial) {
	struct waylandhelper *wlh = data;
	struct wlh_backend *b = &wlh->backend;
	xdg_surface_ack_configure(surf, serial);
	int oldx = wlh->xres,
		oldy = wlh->yres;
	int changed = b->xdg_surface_changed;
	if (b->xdg_surface_changed && b->buffer) {
		b->xdg_surface_changed = 0;
		wlh->xres = b->pending_x;
		wlh->yres = b->pending_y;
		wl_buffer_destroy(b->buffer);
		b->buffer = NULL;
		wlh->res_changed = 1;
	}
	if (!b->buffer)
		b->buffer = attach_imagebuffer(data);
	wl_surface_attach(b->surface, b->buffer, 0, 0);
	if (b->xdgfirst)
		wl_surface_commit(wlh->backend.surface), b->xdgfirst = 0;
	if (wlh->resolution_callback && changed)
		wlh->resolution_callback(wlh, oldx, oldy);
}

static struct xdg_surface_listener xdgsurflistener = {
	.configure = xdgconfigure,
};

/*******************************/

static void topconfigure(void* data, struct xdg_toplevel* top, int32_t w, int32_t h, struct wl_array* states) {
	struct waylandhelper *wlh = data;
	struct wlh_backend *b = &wlh->backend;
	b->pending_x = w<wlh->xresmin ? wlh->xresmin : w;
	b->pending_y = h<wlh->yresmin ? wlh->yresmin : h;
	b->xdg_surface_changed = wlh->xres != b->pending_x || wlh->yres != b->pending_y;
}

static void xdgclose(void* data, struct xdg_toplevel* top) {
	((struct waylandhelper*)data)->stop = 1;
}

static struct xdg_toplevel_listener xdgtoplistener = {
	.configure = topconfigure,
	.close = xdgclose,
};

/*******************************/

static void init_xdg(struct waylandhelper *wlh) {
	struct wlh_backend *b = &wlh->backend;
	assert((b->xdgsurf = xdg_wm_base_get_xdg_surface(b->xdg_base, b->surface)));
	xdg_surface_add_listener(b->xdgsurf, &xdgsurflistener, wlh);

	assert((b->xdgtop = xdg_surface_get_toplevel(b->xdgsurf)));
	xdg_toplevel_add_listener(b->xdgtop, &xdgtoplistener, wlh);

	wl_surface_commit(b->surface);
	b->xdgfirst = 1;
}
