#include <sys/mman.h> // mmap, shm_*
#include <unistd.h> // ftruncate
#include <fcntl.h> // O_*
#include <err.h>
#include <sys/time.h>
#include <stdio.h>

static int _init_shm_file(int koko) {
	char name[48];
	struct timeval t;
	gettimeofday(&t, NULL);
	sprintf(name, "wl_%lx%lx", (long unsigned)t.tv_sec, (long unsigned)t.tv_usec/1000);
	int fd = shm_open(name, O_CREAT | O_RDWR, 0600);
	if (fd < 0)
		err(1, "shm_open %s", name);
	shm_unlink(name); // File stays until fd is closed.
	if (ftruncate(fd, koko) < 0)
		err(1, "ftruncate %i: %s, koko = %i t", fd, name, koko);
	return fd;
}

static void destroy_imagebuffer(struct waylandhelper*);

/* Called from xdgconfigure */
static struct wl_buffer* attach_imagebuffer(struct waylandhelper *wlh) {
	destroy_imagebuffer(wlh);
	struct wlh_backend *b = &wlh->backend;
	b->imagebuffer_size = wlh->xres*wlh->yres*4;
	int fd = _init_shm_file(b->imagebuffer_size);
	assert(fd >= 0);
	wlh->data = mmap(NULL, b->imagebuffer_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (wlh->data == MAP_FAILED)
		err(1, "mmap %zu (fd=%i) %s: %i", b->imagebuffer_size, fd, __FILE__, __LINE__);
	wlh->redraw = 1;

	struct wl_shm_pool* pool  = wl_shm_create_pool(b->shared_memory, fd, b->imagebuffer_size);
	struct wl_buffer* buffer = wl_shm_pool_create_buffer(pool, 0, wlh->xres, wlh->yres, wlh->xres*4, WL_SHM_FORMAT_ARGB8888);
	wl_shm_pool_destroy(pool);
	close(fd);
	return buffer;
}

static void destroy_imagebuffer(struct waylandhelper *wlh) {
	if (wlh->data) {
		munmap(wlh->data, wlh->backend.imagebuffer_size);
		wlh->data = NULL;
	}
}
